# Rancher Nodes
This template deploys an Ubuntu system with Docker installed. In addition, the Rancher agent will be deployed with the callback url as provided by you. So here you need to already have a server running!

[![Deploy Now](http://azuredeploy.net/deploybutton.png)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fbitbucket.org%2Fhumanspot%2Fazure-rancher%2Fraw%2F532ca103247b0925945982808fe2669e64777bf0%2FNodes%2Fazuredeploy.json)